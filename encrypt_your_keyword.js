const bcrypt = require('bcrypt');
const passWord = 'SomeoneSomewhere'

let saltRound= Math.floor( Math.random() * 10 );

//async method

bcrypt.hash(passWord, saltRound, (err,hash)=>{
    if(err) console.log(err);
    else console.log(hash);
} );

//synchronized method

const result = bcrypt.hashSync(passWord, saltRound);

if(!result) console.log('error');
console.log(result);