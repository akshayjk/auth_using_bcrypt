const bcrypt = require('bcrypt');

// paste a random hash generated from other file on the below variable
const hash_code = '$2b$07$PSwkhCEvePZXE576AHWn0eZDubWZB/WCJAxhrjC1iXmIRJknON3CS';

// async method

bcrypt.compare('SomeoneSomewhere',hash_code, (err, flag)=>{
    if(flag) console.log('authenticated');
    else console.log('hacker');
});

//sync method

const flag = bcrypt.compareSync('SomeoneSomewhere', hash_code);

if(flag) console.log('authenticated');
else console.log('hacker');